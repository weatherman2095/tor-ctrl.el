;;; tor-ctrl.el --- An Emacs Tor Control helper  -*- lexical-binding:t -*-

;; Copyright (C) 2023 weatherman2095

;; Author: weatherman2095 (weatherman2095@netcourrier.com)
;; Version: 0.1
;; Package-Requires: ((emacs "25.1") (cl-lib "1.0"))
;; Keywords: Tor, Internet, Whonix

;; This file is not part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Tor, in particular when used with Whonix, is a very useful
;; anonymization network with a number of tools involved in
;; interacting with it.

;; I found that it would be practical to be able to request circuit
;; renewal from within Emacs when using it inside of a Whonix
;; Workstation.

;; The usefulness of this package outside of a Whonix Workstation is
;; limited, as most packages do not use Emacs' `socks.el' SOCKS proxy
;; support, so unless it was started wrapped by torify or torsocks
;; then much of Emacs traffic will not be routed through Tor.

;; It could however still affect other programs on the same system
;; that are using Tor.

;; For more information on Tor, visit the website at
;; <https://www.torproject.org/>.

;; For more information on Whonix, visit the website at
;; <https://www.whonix.org/>

;; Advisory note: This package by default also relies on the iproute2
;; command line tools. If your distribution uses different tools,
;; `tor-ctrl--route-command' and `tor-ctrl--route-args' will need to
;; be modified accordingly.

;; Usage:

;; To connect to the Tor Controller or onion-grater port and request a
;; new Tor circuit.
;;
;; M-x tor-ctrl-new-circuit RET

;;; Code:

(require 'cl-lib)

(defvar tor-ctrl--host nil
  "Host to use for `tor-ctrl' connection.

Default is `nil' to retrieve the gateway as a Whonix setup would
use.

Set to an appropriate localhost address to control `Tor' on the
same host.")

(defvar tor-ctrl--buffer-name " *tor-ctrl*"
  "Name of the buffer `tor-ctrl' will use to keep track of the request state.")

(defvar tor-ctrl--process-name "*tor-ctrl*"
  "Name of the one `tor-ctrl' network process to be used for requests.")

(defvar tor-ctrl--port 9051
  "Tor Controller or Whonix onion-grater port to use by `tor-ctrl'.

Default value is onion-grater port on whonix.")

(defvar tor-ctrl--route-command "ip"
  "Program for `tor-ctrl' to use to retrieve the default routing table.")
(defvar tor-ctrl--route-args '("route")
  "Arguments for `tor-ctrl' to use to retrieve the default routing table.")

(defun tor-ctrl--get-gateway ()
  "Retrieve the IPv4 address of the default gateway, which on
Whonix would be the Whonix-Gateway."
  (with-temp-buffer
    (apply #'call-process tor-ctrl--route-command nil t nil tor-ctrl--route-args)
    (setf (point) (point-min))
    (search-forward-regexp  ".* \\([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\\) .*" nil t)
    (match-string 1)))

(defun tor-ctrl--newnym-filter (proc string)
  "Handle input `string' of the `tor-ctrl' `proc' connection with
normal accumulation.

When a request has received a response, handle it and cleanup
resources."
  (internal-default-process-filter proc string)
  (cl-destructuring-bind (finished . result)
      (with-current-buffer (process-buffer proc)
	(save-excursion
	  (cons (progn
		  (setf (point) (point-min))
		  (search-forward "" nil t))
		(progn
		  (setf (point) (point-min))
		  (search-forward "250 OK" nil t)))))
    (when finished
      (unwind-protect
	  (if result
	      (message "Tor NEWNYM successfully sent")
	    (error "Tor NEWNYM failed"))
	(let ((buf (process-buffer proc)))
	  (delete-process proc)
	  (kill-buffer buf))))))

(defun tor-ctrl-new-circuit ()
  "Send a NEWNYM request to the configured Tor controller to obtain
a new circuit on new connections.

The variable defaults assume a Whonix environment."
  (interactive)
  (when (get-process tor-ctrl--process-name)
    (error "`%s' process already active" tor-ctrl--process-name))
  (make-network-process :name tor-ctrl--process-name
			:buffer (get-buffer-create tor-ctrl--buffer-name)
			:host (or tor-ctrl--host (tor-ctrl--get-gateway))
			:service tor-ctrl--port
			:filter #'tor-ctrl--newnym-filter
			:nowait nil)
  (send-string (get-process tor-ctrl--process-name) "signal NEWNYM\n"))

(provide 'tor-ctrl)
;;; tor-ctrl.el ends here
